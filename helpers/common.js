let resultArray = [];
let clientIdsArray = [];
let requestIdsAssigned=[];
const sum_recursive = (items, target, tmpArray, start) => {
  if (start) {
    resultArray = [];
    clientIdsArray = [];
    requestIdsAssigned=[];
  }
  var s = 0;
  let tempObject = {};
  tmpArray.forEach(function(entry) {
    s = s + entry.hours;
  });

  if (s == target) {
    var res = tmpArray.join();
    res = res.replace(/,/g, "+");
    res = res + "=" + s;
    const getRequestIds = tmpArray.map(rId => rId.requestId);
    const getClientIds = tmpArray.map(rId => rId.clientId);
    let addReqIds=true;
    getRequestIds.forEach((reqId)=>{
      if(requestIdsAssigned.includes(reqId)){
        addReqIds=false;
      }else{
        requestIdsAssigned.push(reqId);
      }
    })
    if(addReqIds){
      tempObject["requests"] = getRequestIds;
      resultArray.push(tempObject);
      let uniqueClientIds = getClientIds.filter(unique);
      uniqueClientIds.forEach(unqId => {
        if (!clientIdsArray.includes(unqId)) {
          clientIdsArray.push(unqId);
        }
      });
    }
  }

  if (s >= target) {
    return;
  }

  for (var i = 0; i < items.length; i++) {
    n = items[i];
    clientsId = n.clientId;
    var remaining = [];
    var remaining = items.slice(i + 1);
    var temp_arr = tmpArray.slice();
    temp_arr.push(n);
    sum_recursive(remaining, target, temp_arr, false);
  }
  
  return { resultArray, clientIdsArray };
};

const unique = (item, index, array) => {
  return array.indexOf(item) == index;
};

const comparer = otherArray => {
  return function(current) {
    return (
      otherArray.filter(function(other) {
        return other.requestId == current.requestId;
      }).length == 0
    );
  };
};

const createPair = records => {
  var perChunk = 2;
  var pairs = records.reduce((pairArray, item, index) => {
    const chunkIndex = Math.floor(index / perChunk);

    if (!pairArray[chunkIndex]) {
      pairArray[chunkIndex] = []; // start a new chunk
    }

    pairArray[chunkIndex].push(item);

    return pairArray;
  }, []);

  const makeRequestArray = pairs.map(pair => {
    return { requests: pair };
  });
  return makeRequestArray;
};

const groupBy = (xs, f) => {
  return xs.reduce(
    (r, v, i, a, k = f(v)) => ((r[k] || (r[k] = [])).push(v), r),
    {}
  );
};

const handleRestData = (requestData, getResult, helper) => {
  const arrayToCompare = [];
  const pairs = [];
  const clientIds = [];
  getResult.resultArray.forEach(item => {
    item.requests.forEach(itemReq => {
      arrayToCompare.push({ requestId: itemReq });
    });
  });

  const comparer = helper.comparer;
  const restItems = requestData.filter(comparer(arrayToCompare));
  const restRequestIds = restItems.map(restId => {
    return restId.requestId;
  });

  const restClientIds = restItems
    .map(restId => {
      return restId.clientId;
    })
    .filter(helper.unique);

  const createThePair = helper.createPair(restRequestIds);
  createThePair.forEach(pair => {
    pairs.push(pair);
  });

  restClientIds.map(clientId => {
    if (!getResult.clientIdsArray.includes(clientId)) {
      getResult.clientIdsArray.push(clientId);
    }
  });
  return pairs;
};

const getHours=(records,key)=>{
  let hours=0;
  records.forEach((item)=>{
    if(item.requestId==key){
      hours=item.hours;
    }
  });
  return hours;
}

module.exports = {
  sum_recursive,
  comparer,
  createPair,
  unique,
  groupBy,
  handleRestData,
  getHours
};
