/* Import NPM Modules*/
const http = require("http");
const express = require("express");
var app = express();
const server = http.createServer(app);
const path = require("path");

// Import Own Defined Modules
let JobController = require("./controllers/JobController");

// App Configuration
app.use(express.static(path.join(__dirname, "public")));

// Add Routes
const router = require("express").Router();
app.use(router);
router.get("/", JobController.handleJobs);
router.post("/", JobController.handleJobs);
// Assign Port
var port = 8080;

server.listen(port, () => {
  console.log("App listening on port " + port);
});

module.exports = router;
