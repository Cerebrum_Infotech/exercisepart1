const helper = require("../helpers/common");

const JobController = {
  handleJobs: async (req, res) => {
    try {
      let exampleRequests = [
        {
          clientId: 1,
          requestId: "abc",
          hours: 4
        },
        {
          clientId: 2,
          requestId: "ghi",
          hours: 2
        },
        {
          clientId: 1,
          requestId: "def",
          hours: 4
        },
        {
          clientId: 1,
          requestId: "zzz",
          hours: 6
        }
      ];

      if (
        req.body &&
        req.body.exampleRequests &&
        req.body.exampleRequests.length
      ) {
        exampleRequests = req.body.exampleRequests;
      }

      let tmpArray = [];
      let finalArray = [];
      let butlers = [];
      let clientIds = [];
      let restData=[];
      let start = true;

      // creating client ids
      const allClientIds = exampleRequests
        .map(clientData => {
          return clientData.clientId;
        })
        .filter(helper.unique);
      
        // applying group by client id
      let updateExampleRequets = helper.groupBy(exampleRequests, c => c.clientId);

      await Promise.all(
        allClientIds.map(async itemId => {
          const requestData = updateExampleRequets[itemId];
          let initialHours = 0;
          
          requestData.forEach(getHour => {
            initialHours = initialHours + parseInt(getHour.hours);
          });

          if (initialHours >= 8) {
            initialHours = 8;
          }

          // fetching the result
          const getResult = await helper.sum_recursive(
            requestData,
            initialHours,
            tmpArray,
            start
          );
  
          // handling the rest data which was not assigned to any butler because of exceeding max time.
          const getRestData = helper.handleRestData(
            requestData,
            getResult,
            helper
          );

          if (getRestData) {
            getRestData.forEach(restDataItem => {
              restDataItem.requests.forEach(restdId=>{
                restData.push(restdId);
              })
            });
          }

          // pushing the result to the main array
          finalArray.push(getResult);
        })
      );

      finalArray.forEach(finalData => {
        finalData.resultArray.forEach(butlerData => {
          // if there is any data which is not assigned to any butler. check other butlers which have less time jobs, assigned that jobs to them.
          if(restData && restData.length){
            let getTotalHours=0;
            butlerData.requests.forEach((Bdata)=>{
              getTotalHours+=helper.getHours(exampleRequests,Bdata);
            });
            
            restData.forEach((Rdata,index)=>{
              const getRestHour=helper.getHours(exampleRequests,Rdata);
              if((getTotalHours + getRestHour) <=8){
                butlerData.requests.push(Rdata);
                getTotalHours+=getRestHour;
                delete restData[index];
              }
            });
          }
          
          butlers.push(butlerData);
        });

        finalData.clientIdsArray.forEach(clientDataId => {
          clientIds.push(clientDataId);
        });
      });

      if(restData && restData.length){
        restData.forEach((data)=>{
          butlers.push({requests:[data]});
        })
      }

      res.json({ butlers, spreadClientIds: clientIds });
    } catch (e) {
      console.log(e);
    }
  }
};

module.exports = JobController;
